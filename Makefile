SHELL=/bin/sh

## Colors
ifndef VERBOSE
.SILENT:
endif
PHP_VERSIONS := 7.3
PHP_VERSION ?= $(lastword $(sort $(PHP_VERSIONS)))
COLOR_COMMENT=\033[0;32m
IMAGE_PATH=/betd/public/docker/phpqa
REGISTRY_DOMAIN=registry.gitlab.com
VERSION:=$(shell git ls-remote --tags https://github.com/jakzal/toolbox.git | sort -t '/' -k 3  -V|cut -d "/" -f 3|grep -o -E '[0-9]{0,}\.[0-9]{0,}\.[0-9]{0,}'|tail -n 1)
IMAGE=${REGISTRY_DOMAIN}${IMAGE_PATH}:${VERSION}
IMAGE_EXISTS:=$(shell docker pull ${IMAGE} > /dev/null && echo 1 || echo 0)

## Help
help:
	printf "${COLOR_COMMENT}Usage:${COLOR_RESET}\n"
	printf " make [target]\n\n"
	printf "${COLOR_COMMENT}Available targets:${COLOR_RESET}\n"
	awk '/^[a-zA-Z\-\_0-9\.@]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf " ${COLOR_INFO}%-16s${COLOR_RESET} %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)

## build and push image
all: build push_image

## build image and tags it
build:
	@for php_version in $(PHP_VERSIONS); do \
		docker build -f $$php_version/Dockerfile . -t ${IMAGE}-php$$php_version -t ${REGISTRY_DOMAIN}${IMAGE_PATH}:php$$php_version; \
		if [ $(PHP_VERSION) = $$php_version ]; then \
			printf "Tag latest version"; \
			docker build -f $$php_version/Dockerfile . -t ${REGISTRY_DOMAIN}${IMAGE_PATH}:latest; \
		fi \
	done

## login on registry
registry_login:
ifeq ($(CI_BUILD_TOKEN),)
	docker login ${REGISTRY_DOMAIN}
else
	printf "Docker login from CI with ${CI_REGISTRY_USER} to ${CI_REGISTRY}\n"
	docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
endif

build-if-not-exists:
	@for php_version in $(PHP_VERSIONS); do \
		if [ $(shell docker pull ${REGISTRY_DOMAIN}${IMAGE_PATH}:php$$php_version > /dev/null && echo 1 || echo 0) = 0 ]; then \
			docker build -f $$php_version/Dockerfile . -t ${IMAGE}-php$$php_version -t ${REGISTRY_DOMAIN}${IMAGE_PATH}:php$$php_version; \
			if [ $(PHP_VERSION) = $$php_version ]; then \
				printf "Tag latest version"; \
				docker build -f $$php_version/Dockerfile . -t ${REGISTRY_DOMAIN}${IMAGE_PATH}:latest; \
			fi \
		fi \
	done

## push image
push_image-if-not-exists: registry_login
	for php_version in $(PHP_VERSIONS); do \
		if [ $(shell docker pull ${REGISTRY_DOMAIN}${IMAGE_PATH}:php$$php_version > /dev/null && echo 1 || echo 0) = 0 ]; then \
			docker push ${IMAGE}-php$$php_version; \
			docker push ${REGISTRY_DOMAIN}${IMAGE_PATH}:php$$php_version; \
		fi \
	done
	docker push ${REGISTRY_DOMAIN}${IMAGE_PATH}:latest

## pull image
pull_image: registry_login
	docker pull ${IMAGE}

## push image
push_image: registry_login
	for php_version in $(PHP_VERSIONS); do \
		docker push ${IMAGE}-php$$php_version; \
		docker push ${REGISTRY_DOMAIN}${IMAGE_PATH}:php$$php_version; \
	done
	docker push ${REGISTRY_DOMAIN}${IMAGE_PATH}:latest

test:
	for php_version in $(PHP_VERSIONS); do \
		docker run --rm ${IMAGE}-php$$php_version noverify --version; \
		docker run --rm ${IMAGE}-php$$php_version phpunit-8 --version; \
		docker run --rm ${IMAGE}-php$$php_version phpunit-7 --version; \
		docker run --rm ${IMAGE}-php$$php_version phpunit-5 --version; \
		docker run --rm ${IMAGE}-php$$php_version yaml-lint --version; \
		docker run --rm ${IMAGE}-php$$php_version twig-lint --version; \
		docker run --rm ${IMAGE}-php$$php_version twigcs --version; \
		docker run --rm ${IMAGE}-php$$php_version phpcpd --version; \
		docker run --rm ${IMAGE}-php$$php_version phpmd --version; \
		docker run --rm ${IMAGE}-php$$php_version phpcs --version; \
		docker run --rm ${IMAGE}-php$$php_version php-cs-fixer --version; \
		docker run --rm ${IMAGE}-php$$php_version phpstan --version; \
		docker run --rm ${IMAGE}-php$$php_version phpmetrics --version; \
	done
